----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:07:02 06/26/2014 
-- Design Name: 
-- Module Name:    AD7415  - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity AD7415_ch3 is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           temp_data3_7415 : out  STD_LOGIC_VECTOR (15 downto 0);
           sda_ch3 : inout  STD_LOGIC;
           sclk_7415_ch3 : out  STD_LOGIC
		  );
end AD7415_ch3;

architecture Behavioral of AD7415_ch3 is

signal delay_count:integer;
signal sclkby3_count:integer;
signal repeat_twice00:integer;
signal sh_int_counter:integer;
signal sh_int_counter_2:integer;
signal sh_countfor_data:integer;
signal repeat_ones:integer;
signal sclk_reg1:STD_LOGIC;
signal delay_800ms:integer;
signal sclkby3:STD_LOGIC;
signal sck_posedge:STD_LOGIC;
signal sck_negedge:STD_LOGIC;
signal sck_buffer:STD_LOGIC;
signal ack_byslave:STD_LOGIC;
signal sda_buffer:STD_LOGIC;
signal data_latch:STD_LOGIC;
signal sck_negedge_delay:STD_LOGIC;
signal sh_in15:STD_LOGIC_VECTOR(15 downto 0);
signal MOSI_AD7415_ext:STD_LOGIC_VECTOR(7 downto 0);
signal MOSI_LTC2440_ext:STD_LOGIC_VECTOR(7 downto 0);
signal sh_in:STD_LOGIC_VECTOR(15 downto 0);
signal data_7415:STD_LOGIC_VECTOR(9 downto 0);
signal state:integer;
signal receive_data:integer;
signal delay_sck:integer;
signal sclk_count_for16data:integer;
signal sh_out:integer;
signal sh_repeat:integer;
signal delay_stop:integer;
signal delay_zero:integer;
--signal sh_int_counter:integer;



begin

-------------------------------delay 800 ms--------------------
process (clk,rst)

begin
     if(rst = '1') then
		 delay_count <= 0;
			 
     elsif(rising_edge(clk)) then
				
	    if (delay_count = 40000) then --800us delay
             delay_count <= 0;
		     delay_800ms <= 1; 
  
	    else 
			 delay_count <= delay_count + 1;
			 delay_800ms <= 0; --idle
 
        end if; 			
	 end if;	
 
end process;

------------------------------------------------------------




 

------------------------------------------------------------ clk by 3------------
process(clk,rst)
 begin
	 
	 if(rst='1') then
		 sclk_reg1 <='0';
			
	 elsif rising_edge(clk) then
		 sclk_reg1  <= sclkby3;
	 end if;
end process;

		
 sck_posedge <= not sclk_reg1 and  sclkby3;
 sck_negedge <=   sclk_reg1 and  not sclkby3;
			
			
 SRL16_inst_1 : SRL16
   generic map (
      INIT => X"0000")
   port map (
      Q => sck_negedge_delay,       -- SRL data output -- adds a delay of 16*20ns 
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '1',     -- Select[3] input
      CLK => CLK,   -- Clock input
      D => sck_negedge        -- SRL data input
   );
	
	
-------------------------------------------SCK generate-----------------------------


process (clk,rst)
begin
     if (rst = '1') then 
		 state <= 0;
         sclkby3 <= '1';
		 repeat_twice00 <= 0;
		 delay_sck <= 0;
         sclk_count_for16data <= 0;
     elsif (rising_edge(clk)) then

	    case state is
				
		    when 0 =>
							
				 sclkby3 <= '1';
			        if (delay_800ms = 1) then
						 state <= 1;
														
				     end if;
		    when 1 =>	
						   
								 
				 if (delay_sck = 19) then
					 delay_sck <= 0;
					 state <= 2;
                     sclkby3 <= '0';
				 else
					 delay_sck <= delay_sck + 1;
					 sclkby3 <= '1';
		         end if;
		    when 2 =>
--									
									
				 if (sclkby3_count = 63) then
					 sclkby3 <= not sclkby3;
					 sclkby3_count <= 0; 
					 sclk_count_for16data <= sclk_count_for16data + 1;
				 else 
					 sclkby3_count <= sclkby3_count + 1;
							
				 end if; 
								
				
				 if (sclk_count_for16data = 56) then
					 sclkby3 <= '1';
					 state <= 0;
					 sclk_count_for16data <= 0;
				 end if;
		    when others => null;				
				end case;
        end if;
end process;

process (rst,clk)
begin
		
if (rst = '1') then

	receive_data <= 0;
	sh_out <= 0;
    MOSI_AD7415_ext <= "10010001";
    sda_buffer <= '1';
	sh_int_counter <= 0;
	sh_int_counter_2 <= 0;
	
elsif (rising_edge (clk)) then	
	
	case receive_data is
		
	    when 0 => 
			
			 sda_buffer <= '1';
		     if (delay_800ms = 1) then
						      
			     receive_data <= 1;
				 MOSI_AD7415_ext<= "10010001";
--				 tempt_data_7415 <= x"0000";
             end if;
							   
	     when 1 =>
			
	                      
			 if (delay_zero = 9) then
				 delay_zero <= 0;
				 sda_buffer <= '0'; 
				 receive_data <= 2;
                             
			 else
				 delay_zero <= delay_zero + 1;
			 end if;
									
							
					                      			
		 when  2 =>
			 if (sck_negedge_delay = '1') then   --this logic is perfect on 18/7/2014
				 sda_buffer <= MOSI_AD7415_ext(7);
				 MOSI_AD7415_ext <= MOSI_AD7415_ext(6 downto 0) & '0';
				 sh_out <= sh_out + 1;
			 
			 if(sh_out = 8) then	
				 receive_data <= 3;
				 sh_out <= 0;
			 end if;	
--		          sda <= sda_buffer;
		     end if;
	     
		 when 3 =>
			 
			 if (sck_negedge_delay = '1') then
				 ack_byslave <= sda_ch3;	
--			     ack_byslave <= '0';	
                 sda_buffer <= '1';
	
		    if (ack_byslave = '0') then
			    receive_data <= 4;  --receive acknowledgement from slave
									
		    else 
                 receive_data <= 0;  								
		    end if;							 
		  end if;					
							
			when 4 => 
		        
				if (sck_negedge_delay = '1') then
					   	 
                    sh_in <= sh_in(14 downto 0) & sda_ch3; 
					 sda_buffer <= '1';
							 
				 if (sh_int_counter = 7) then
							        
					 sh_int_counter <= 0;
                     sda_buffer <= '0';
				     receive_data <= 6;
			    else 
                    sh_int_counter <= sh_int_counter + 1;	 										
			    end if;
			   end if;
			
			when 6 => 
			    if (sck_negedge_delay = '1') then
					 sh_in <= sh_in(14 downto 0) & sda_ch3;
				     sda_buffer <= '1';
				 if (sh_int_counter_2 = 7) then
							        
					 sh_int_counter_2 <= 0;
					 receive_data <= 7;


				 else 
                     sh_int_counter_2 <= sh_int_counter_2 + 1;	 										
					 sda_buffer <= '1'; 
				 end if;
		         end if;
						
			
			when 7 =>
				 if (sck_negedge_delay = '1') then
					 sda_buffer <= '1';
					 receive_data <= 8;
				 end if;					

			when 8 =>
							if (sck_negedge_delay = '1') then
								sda_buffer <= '0';
								receive_data <= 9;
							end if;		
							
         when 9 =>
			  if (delay_stop = 80) then
				 delay_stop <= 0;
				 sda_buffer <= '1'; --for stop 
				 receive_data <= 0;
                             
			  else
				 delay_stop <= delay_stop + 1;
			 end if;
									

						
			when others => null;				
--     sda <= sda_buffer;
end case;


end if;

end process;		  
--								
						

sclk_7415_ch3 <= 'Z' when sclkby3 = '1' else '0';
sda_ch3 <= 'Z' when sda_buffer = '1' else '0';
 
data_7415 <= sh_in(15 downto 8) & sh_in(7 downto 6);
  temp_data3_7415 <= data_7415(9) & data_7415(9) & data_7415(9) & data_7415(9) & data_7415(9) & data_7415(9) &data_7415;
  
end Behavioral; 

